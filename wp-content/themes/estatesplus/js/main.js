(function($){
  
  var site = {
    rate : 250,
    op : 0.3,
    prefix : '/'
  };
  
  var isMobile = function() {
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/Blackberry/i)) {
        return true;
      } else {
        return false;
      }
  };
  
  $(document).ready(function(){
    
    $(window).hashchange( function(){
      var hash = location.hash.substr(3);
      if(hash){
        console.log(hash);
        $('#filter-list a').removeClass('active');
        $('#filter-list a[href="#!/'+hash+'"]').addClass('active');
        $('.list-wrapper').height( $('#property-list').height() );
        $('#property-list article:visible').fadeOut('fast',function(){
          if( $('#property-list article:animated').length === 0 ){
            $('#property-list article.'+hash).fadeIn('fast');
          }
        });
      }
    });
    $(window).hashchange();

    if( $('#map').length ){
      
      function initializeMap(location) {
        var venue = new google.maps.LatLng(location.longitude, location.latitude)
        var mapOptions = {
          center: venue,
          zoom: 11,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoomControl: true,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT
          }
        };

        var map = new google.maps.Map(document.getElementById("map"),mapOptions);

        var marker = new google.maps.Marker({
          position: venue,
          map: map,
          animation: google.maps.Animation.DROP,
          title:location.title
        });

      }
      
      var loc = {
        title: $('#map').data('title'),
        longitude: $('#map').data('longitude'),
        latitude: $('#map').data('latitude')
      };
      initializeMap(loc);

    }
    
    $.cookieCuttr({
      cookieAnalytics: false,
      cookieDomain: "estatesplus.com",
      cookieDeclineButton: false,
      cookieWhatAreTheyLink: 'http://www.bbc.co.uk/webwise/guides/about-cookies',
      cookiePolicyLink: site.prefix + 'cookie-policy/',
      cookieMessage: "<p>We use cookies to collect statistics on how the site is used. No personal information is stored. <a href=\"{{cookiePolicyLink}}\" title=\"Find out more\">Find out more</a>.</p>",
      cookieAcceptButtonText: "Allow"
    });
    
    /* Splash Page */
    if( $('#splash-carousel').length ){
      
      $('#splash-carousel').cycle({
        slides : '> .slide',
        timeout : 0,
        prev : '#splash-navigation .cycle-prev',
        next : '#splash-navigation .cycle-next',
        captionPlugin : 'caption2',
        captionFxIn : 'fadeIn',
        captionFxOut : 'fadeOut',
        caption : '#splash-navigation .cycle-overlay',
        captionTemplate : '<h1>{{cycleTitle}} &mdash; {{cycleDesc}}</h1><p><a href="{{cycleUrl}}">View project +</a>'
      });
      
    }
    
    if ($("#archives").length) {
        if ($("#news").data("year")){
            var $li = $("#archives span:contains(" + $("#news").data("year") + ")"); 
        }else {
            var thedate = new Date;
            var $li = $("#archives span:contains(" + thedate.getFullYear() + ")")
        }
        $li.parent().find("ul").show();
        $li.addClass("active");
        $("#news").data("month") && $li.parent().find("ul a:contains(" + $("#news").data("month") + ")").addClass("active");
        $("#archives span").click(function () {
            $(this).parent().find("ul").slideToggle(site.rate / 2);
            $(this).toggleClass("active");
        });
    }
    
    $('nav.global > ul > li').hover(function(){
      $(this).find('ul.sub-menu').slideDown(site.rate);
    },function(){
      $(this).find('ul.sub-menu').slideUp(site.rate);
    });

    
    
    
  }); // end dom ready
  
})(jQuery);