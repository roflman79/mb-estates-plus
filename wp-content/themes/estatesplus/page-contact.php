<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>

    <section class="content">
      <section class="banner">
        <?php
        $geo = $geo = explode(",",get_field('location', 'option') );
        ?>
        <section id="map" data-longitude="<?php echo $geo[0]; ?>" data-latitude="<?php echo $geo[1]; ?>"></section>
      </section>
      
      <section class="main" role="main">
        <section class="intro">
          <p><?php the_field('introduction'); ?></p>
        </section>
        <aside>
          <p><?php the_field('aside'); ?></p>
        </aside>
        <article>
          <?php the_content() ?>
        </article>
      </section>
      
    </section>
    
    <?php endwhile; endif; ?>

<?php get_footer(); ?>