<?php 

/*
Template Name: Text Page
*/

get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
  
    <section class="content">
      
      <section class="main full" role="main">
        <article>
          <h1><?php the_title(); ?></h1>
          <?php the_content() ?>
        </article>
      </section>
      
    </section>
    
    <?php endwhile; endif; ?>

<?php get_footer(); ?>