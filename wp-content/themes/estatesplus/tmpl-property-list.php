<?php 
/*
Template Name: Property List
*/

get_header(); ?>

    <section class="content">

      <?php 
      if ( have_posts() ) : while ( have_posts() ) : the_post(); 
        $type = $post->post_name;
      ?>
      
      <h1 class="title"><?php the_title(); ?></h1>
      <aside class="sidebar">
        <div id="filter-list">
          <header>Filter by Town</header>
          <?php 
          //$towns_by_cat = array();
          // listing-type
          $args = array(
            'post_type' => 'property',
            //'category_name' => $type,
            'listing-type' => $type,
            'posts_per_page' => -1
          );
          $query = new WP_Query($args);
          while ($query->have_posts()) : $query->the_post();
            
            $terms = get_the_terms( $post->ID, 'town' );
            foreach( $terms as $term ):
              if( !in_array($term->ID, $towns_by_cat) ){
                //array_push($towns_by_cat, $term->term_id);
                $towns_by_cat .= $term->term_id.",";
              }
            endforeach;
           

          endwhile; wp_reset_postdata();


          $args = array(
              'taxonomy' => 'town',
              'include' => substr($towns_by_cat,0,-1)
            );
          $towns = get_categories($args);  
    
          ?>
          <ul>
            <?php foreach($towns as $town): ?>
            <li><a href="#!/<?php echo $town->slug; ?>"><?php echo $town->name; ?></em></a></li>
            <?php endforeach; ?>
          </ul>
        </div>
      </aside>
      
      <section id="property-list">
        <?php the_content(); ?>
        
        <div class="list-wrapper">
        <?php endwhile; endif; ?>

        <?php
        $args = array(
          'post_type' => 'property',
          //'category_name' => $type,
          'listing-type' => $type,
          'posts_per_page' => -1,
          'orderby' => 'menu_order date',
          'order' => 'DESC'
        );
        $query = new WP_Query($args);
        while ($query->have_posts()) : $query->the_post();
          $categories = get_the_terms( get_the_ID(), 'town' );
          foreach($categories as $cat):
            $classes .= $cat->slug." ";
          endforeach;
        ?>
        <article class="<?php echo $classes; ?>">
          <div class="cycle-slideshow" data-cycle-timeout="0">
            <?php 
            $shots = get_field('gallery');
            foreach( $shots as $shot ): ?>
            <img src="<?php echo $shot['sizes']['thumbnail']; ?>" alt="<?php echo $shot['alt']; ?>">
            <?php endforeach; ?>
            <div class="cycle-pager"></div>
          </div>
          <div class="details">
            <h1><?php the_title(); ?></h1>
            <h2><?php the_field('address'); ?></h2>
            <h3><?php if( get_field('cost') ): the_field('cost'); else: echo "Price on application"; endif; ?></h3>
            <?php 
            if( get_field('pdf') ):
              $file = get_field('pdf'); 
            ?>
            <a href="<?php echo $file['url']; ?>" target="_blank" class="read-more">Read more</a>
            <?php endif; ?>
          </div>
        </article>
        <?php $classes = ""; endwhile; ?>
        
        <?php if ( !is_search() ): ?>
        <nav>
          <ul class="postnav">
            <?php if( get_previous_posts_link() ): ?><li class="previous "><?php previous_posts_link( __('Previous') ) ?></li><?php endif; ?>
            <?php if( get_next_posts_link() ): ?><li class="next"><?php next_posts_link( __('Next') ); ?></li><?php endif; ?>
          </ul>
        </nav>
        <?php endif; ?>
        </div>
      </section>
      
    </section>

<?php get_footer(); ?>