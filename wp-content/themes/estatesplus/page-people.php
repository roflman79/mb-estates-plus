<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>

    <section class="content">
      <section class="banner">
        <section class="block red wide">
          <?php the_content() ?>
        </section>
      </section>
      
      <section class="team" role="main">
        
        <?php $i = 1; while( has_sub_field('people') ): ?>
        <section class="member <?php if( $i % 2 ): echo "left"; else: echo "right"; endif; ?>">
          <?php
          $img = get_sub_field('photo');
          if( $img ):
          ?>
          <img src="<?php echo $img['sizes']['thumbnail'] ?>" alt="" />
          <?php else: ?>
          <div class="blank"></div>
          <?php endif; ?>
          <article>
            <header>
              <h1><span><?php the_sub_field('name'); ?></span> <?php the_sub_field('quals'); ?></h1>
              <h2><?php the_sub_field('role'); ?></h2>
            </header>
            <ul>
              <?php if( get_sub_field('ddi') ): ?><li><span>DDI:</span> <?php the_sub_field('ddi'); ?></li><?php endif; ?>
              <?php if( get_sub_field('mobile') ): ?><li><span>Mob:</span> <?php the_sub_field('mobile'); ?></li><?php endif; ?>
              <?php if( get_sub_field('email') ): ?><li><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></li><?php endif; ?>
            </ul>
          </article>
        </section>
        <?php $i++; endwhile; ?>
        
      </section>
      
    </section>
    
    <?php endwhile; endif; ?>

<?php get_footer(); ?>