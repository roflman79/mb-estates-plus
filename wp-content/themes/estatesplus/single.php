<?php get_header(); ?>

    <section class="content">

      <h1 class="title">The latest news and views</h1>

      <aside class="sidebar">
        <header>
          <a href="<?php echo get_permalink(17); ?>">Back to news</a>
        </header>
      </aside>
      
      <section id="single">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
        <article>
          <h1><?php the_title(); ?></h1>
          <?php
          $img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'news');
          if( $img ):
          ?>
          <img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="" />
          <?php endif; ?>
          <?php the_content(); ?>
        </article>
        <?php endwhile; endif; ?>
        
      </section>
      
    </section>

<?php get_footer(); ?>