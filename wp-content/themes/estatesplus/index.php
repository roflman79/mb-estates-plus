<?php get_header(); ?>

    <section class="content">

      
      <?php if( is_date() ): ?>
      <h1 class="title">Archive for <?php the_time('F Y'); ?></h1>
    <?php elseif( is_category() ): ?>
      <h1 class="title">Archive for <?php single_cat_title( '', true ); ?></h1>
      <?php else: ?>
      <h1 class="title">The latest news and views</h1>
      <?php endif; ?>

      <aside class="sidebar">
        <div class="widget-categories">
          <header>Categories</header>
          <ul>
            <?php wp_list_categories('title_li=&hide_empty=0&exclude=1'); ?>
          </ul>
        </div>
        <div class="widget-archive">
          <header>
            <?php if( is_date() ): ?>
            <a href="<?php echo get_permalink(17); ?>">Back to news</a>
            <?php else: ?>
              Archive
            <?php endif; ?>
          </header>
          <div class="archives">
            <ul>
              <li><?php wp_get_archives(); ?></li>
            </ul>
            <?php //wp_custom_archive(); ?>
          </div>
        </div>
      </aside>
      
      <section id="news" <?php if( is_month() ) echo ' data-month="'.get_the_time('F').'"'; ?><?php if( is_month() ) echo ' data-year="'.get_the_time('Y').'"'; ?>>
        <!-- <p>To keep up to date with all the news, views and updates from Meller Braggins Estates Plus why not sign up to our newsletter? <a href="#">Subscribe today</a></p> -->
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
        <article>
          <a href="<?php the_permalink(); ?>" class="img">
            <?php
            $img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'feature2');
            if( $img ):
            ?>
            <img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="" />
            <?php endif; ?>
          </a>
          <div>
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            <div class="details">
              <p class="date"><?php the_time( get_option('date_format') ); ?></p>
              <p class="categories"><?php the_category(' '); ?></p>
            </div>
            <a href="<?php the_permalink(); ?>" rel="nofollow" class="read-more">Read more</a>
          </div>
        </article>
        <?php endwhile; endif; ?>
        
        <?php if ( !is_search() ): ?>
        <nav>
          <ul class="postnav">
            <?php if( get_previous_posts_link() ): ?><li class="previous "><?php previous_posts_link( __('Previous') ) ?></li><?php endif; ?>
            <?php if( get_next_posts_link() ): ?><li class="next"><?php next_posts_link( __('Next') ); ?></li><?php endif; ?>
          </ul>
        </nav>
        <?php endif; ?>
        
      </section>
      
    </section>

<?php get_footer(); ?>