<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
  
    <section class="content">
      <!-- about -->
      <section class="banner">
        <?php 
        $img = get_field('top_image'); 
        if( $img ):
        ?>
        <img src="<?php echo $img['sizes']['feature1']; ?>" width="<?php echo $img['sizes']['feature1-width']; ?>" height="<?php echo $img['sizes']['feature1-height']; ?>" alt="" />
        <?php else: ?>
        <div class="placeholder feature1"></div>
        <?php endif; ?>
        <section class="feature">
          <h1><?php the_title(); ?></h1>
          <p><?php the_field('caption'); ?></p>
        </section>
        <blockquote class="block grey">
          <?php if( get_field('header_quote') ): ?>
          <p>&ldquo;<?php the_field('header_quote'); ?>&rdquo;</p>
          <?php endif; ?>
          <?php if( get_field('quote_footer') ): ?>
          <footer>
            <p><?php the_field('quote_footer'); ?></p>
          </footer>
          <?php endif; ?>
          <?php if( get_field('lettings_list') ): ?>
            <p class="dl"><a href="<?php the_field('lettings_list'); ?>" target="_blank">Download Lettings List</p>
          <?php endif; ?>
          <?php if( get_field('land_sales') ): ?>
            <p class="dl"><a href="<?php the_field('land_sales'); ?>" target="_blank">Download Land Sales Particulars</p>
          <?php endif; ?>
        </blockquote>
        <?php 
        $img = get_field('bottom_image'); 
        if( $img ):
        ?>
        <img src="<?php echo $img['sizes']['feature2'] ?>" width="<?php echo $img['sizes']['feature2-width']; ?>" height="<?php echo $img['sizes']['feature2-height']; ?>" alt="" />
        <?php else: ?>
        <div class="placeholder feature2"></div>
        <?php endif; ?>
      </section>
      
      <section class="main" role="main">
        <section class="intro">
          <p><?php the_field('introduction'); ?></p>
        </section>
        <aside>
          <p><?php the_field('aside'); ?></p>
        </aside>
        <article>
          <?php the_content() ?>
        </article>
      </section>
      
    </section>
    
    <?php endwhile; endif; ?>

<?php get_footer(); ?>