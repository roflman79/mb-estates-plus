<?php

if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'square', 470, 470, true );
  add_image_size( 'banner', 940, 340, true );
  add_image_size( 'casestudy', 411, 300, true );
  add_image_size( 'feature1', 470, 340, true );
  add_image_size( 'feature2', 470, 235, true );
  add_image_size( 'news', 705, 400, true );
}

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
		  'main_menu' => 'Main Menu',
		  'footer_menu' => 'Footer Menu'
		)
	);
}

/*
  Enqueue Scripts and Styles
  **************************************************************/

if( !is_admin()){
  // JS
  wp_deregister_script('jquery');
  wp_enqueue_script('theme_typekit', '//use.typekit.net/edn6bss.js');
  wp_enqueue_script('jquery', "//code.jquery.com/jquery-1.11.0.min.js", true, null, false);
  wp_enqueue_script('jquerymigrate', "//code.jquery.com/jquery-migrate-1.2.1.min.js", true, null, false);

  wp_enqueue_script('cycle2', get_bloginfo('template_directory')."/js/vendor/jquery.cycle2.min.js", false, '1.0', true);
  wp_enqueue_script('cycle2caption', get_bloginfo('template_directory')."/js/vendor/jquery.cycle2.caption2.min.js", false, '2.0', true);
  wp_enqueue_script('hashchange', get_bloginfo('template_directory')."/js/vendor/jquery.ba-hashchange.min.js", false, '1.3', true);
  wp_enqueue_script('cookies', get_bloginfo('template_directory')."/js/vendor/jquery.cookie.js", false, '1.3.1', true);
  wp_enqueue_script('cookiecuttr', get_bloginfo('template_directory')."/js/vendor/jquery.cookiecuttr.js", false, '1.0', true);
  
  wp_enqueue_script('maps', "https://maps.googleapis.com/maps/api/js?v=3&sensor=true", false, '3.0', true);
  wp_enqueue_script('main', get_bloginfo('template_directory')."/js/main.min.js", false, '1.5', true);
  wp_enqueue_script('modernizr', get_bloginfo('template_directory')."/js/vendor/modernizr-2.6.2.min.js", true, '2.6.2', false);
  // CSS
  wp_enqueue_style( 'styles', get_bloginfo('template_directory')."/css/style.css", false, '1.0', false );
  
  function theme_typekit_inline() {
    if ( wp_script_is( 'theme_typekit', 'done' ) ) { ?>
    	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <?php }
  }
  add_action( 'wp_head', 'theme_typekit_inline' );
  
}

/*
  General Housekeeping
  **************************************************************/

remove_action('wp_version_check', 'wp_version_check');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'rsd_link');
remove_action('admin_init', '_maybe_update_core');

add_filter('pre_transient_update_core', create_function( '$a', "return null;"));
add_action( 'admin_init', create_function('', 'remove_action( \'admin_notices\', \'update_nag\', 3 );') );

/* remove rubbish inline styles from header*/
add_action( 'widgets_init', 'my_remove_recent_comments_style' );
function my_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'  ) );
}


/* Clean up image insertions */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

/* Change auto embed width */
if ( !isset( $content_width ) ){
  
  /*
  if( is_mobile() ){
    $content_width = 310;
  }else{
    $content_width = 432;
  }
  */
  
}

/*
  Custom Archives Function
  **************************************************************/

function wp_custom_archive($args = '') {
    global $wpdb, $wp_locale;
    $defaults = array(
        'limit' => '',
        'format' => 'html', 'before' => '',
        'after' => '', 'show_post_count' => false,
        'echo' => 1
    );
    $r = wp_parse_args( $args, $defaults );
    extract( $r, EXTR_SKIP );
    if ( '' != $limit ) {
        $limit = absint($limit);
        $limit = ' LIMIT '.$limit;
    }
    // over-ride general date format ? 0 = no: use the date format set in Options, 1 = yes: over-ride
    $archive_date_format_over_ride = 0;
    // options for daily archive (only if you over-ride the general date format)
    $archive_day_date_format = 'Y/m/d';
    // options for weekly archive (only if you over-ride the general date format)
    $archive_week_start_date_format = 'Y/m/d';
    $archive_week_end_date_format   = 'Y/m/d';
    if ( !$archive_date_format_over_ride ) {
        $archive_day_date_format = get_option('date_format');
        $archive_week_start_date_format = get_option('date_format');
        $archive_week_end_date_format = get_option('date_format');
    }
    //filters
    $where = apply_filters('customarchives_where', "WHERE post_type = 'post' AND post_status = 'publish'", $r );
    $join = apply_filters('customarchives_join', "", $r);
    $output = '<ul>';
        $query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC $limit";
        $key = md5($query);
        $cache = wp_cache_get( 'wp_custom_archive' , 'general');
        if ( !isset( $cache[ $key ] ) ) {
            $arcresults = $wpdb->get_results($query);
            $cache[ $key ] = $arcresults;
            wp_cache_set( 'wp_custom_archive', $cache, 'general' );
        } else {
            $arcresults = $cache[ $key ];
        }
        if ( $arcresults ) {
            $afterafter = $after;
            //print_r($arcresults);
            $i = 0;
            foreach ( (array) $arcresults as $arcresult ) {
                $url = get_month_link( $arcresult->year, $arcresult->month );
                /* translators: 1: month name, 2: 4-digit year */
                $text = sprintf(__('%s'), $wp_locale->get_month($arcresult->month));
                $output .= ( $arcresult->year != $temp_year && $i!=0 ) ? '</ul></li>' : '';
                $year_text = sprintf('<li><span>%d</span><ul>', $arcresult->year);
                if ( $show_post_count )
                    $after = '&nbsp;('.$arcresult->posts.')' . $afterafter;
                $output .= ( $arcresult->year != $temp_year ) ? $year_text : '';
                $output .= get_archives_link($url, $text, $format, $before, $after);
                $temp_year = $arcresult->year;
                $i++;
            }
        }
    $output .= '</ul></ul>';
    if ( $echo )
        echo $output;
    else
        return $output;
}

/* 
  Twitter Caching Script for Wordpress
  **************************************************************/

function get_tweets($userid,$count=1){
  /* cache the tweets so that no rate limit */
  require_once("twitteroauth/twitteroauth/twitteroauth.php");

  $consumerkey        = "84wU4zowjeWPO703gFvtNw";
  $consumersecret     = "czam06OoIHSiuXRyeSOJ29Wu60bBKyLONWsTZhTp0Y";
  $accesstoken        = "11163702-VCNsxx6qsxIO3OM2fCeuEWd7uAGukmIJEXkiXTpQ";
  $accesstokensecret  = "cnzGhbjynaQM3z7TqAESaXhqIeV5V62pSfyvOrHP83I";
  
  $connection = new TwitterOAuth($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);
  $tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$userid."&count=".$count."&include_rts=1");


  $cache_file = dirname(__FILE__).'/cache/'.'twitter-cache-'.$userid;
  if( !file_exists($cache_file) ){
    $fh = fopen($cache_file, 'w');
    fwrite($fh, "");
    fclose($fh);
  }
  $modified = filemtime( $cache_file );
  $now = time();
  $interval = 500; // 5 mins
  
  if ( !$modified || ( ( $now - $modified ) > $interval ) ) {
    if ( $tweets ) {
      $cache_static = fopen( $cache_file, 'w' ) or die("could not write to file");
      fwrite( $cache_static, json_encode($tweets) );
      fclose( $cache_static );
    }
  }
  
  $json = file_get_contents($cache_file) or die("could not open file");
  
  $obj = json_decode($json);
  $i = 0;
  foreach($obj as $index => $status){
    if( $i < $count ){
      $text = $status->text;
      echo '<div class="tweet">';
      echo '<p>'.format_tweet($text).'</p>';
      echo '<p class="date">'.format_date($status->created_at).'</p>';
      echo '</div>';
      $i++;
    }
  }
  
  
}


function format_date($date) {

	$blocks = array (
		array('year',  (3600 * 24 * 365)),
		array('month', (3600 * 24 * 30)),
		array('week',  (3600 * 24 * 7)),
		array('day',   (3600 * 24)),
		array('hour',  (3600)),
		array('min',   (60)),
		array('sec',   (1))
		);

	$argtime = strtotime($date);
	$nowtime = time();

	$diff    = $nowtime - $argtime;

	$res = array();

	for ($i = 0; $i < count($blocks); $i++) {
		$title = $blocks[$i][0];
		$calc  = $blocks[$i][1];
		$units = floor($diff / $calc);
		if ($units > 0) {
			$res[$title] = $units;
		}
	}

	if (isset($res['year']) && $res['year'] > 0) {
		if (isset($res['month']) && $res['month'] > 0 && $res['month'] < 12) {
			$format      = "About %s %s %s %s ago";
			$year_label  = $res['year'] > 1 ? 'years' : 'year';
			$month_label = $res['month'] > 1 ? 'months' : 'month';
			return sprintf($format, $res['year'], $year_label, $res['month'], $month_label);
		} else {
			$format     = "About %s %s ago";
			$year_label = $res['year'] > 1 ? 'years' : 'year';
			return sprintf($format, $res['year'], $year_label);
		}
	}

	if (isset($res['month']) && $res['month'] > 0) {
		if (isset($res['day']) && $res['day'] > 0 && $res['day'] < 31) {
			$format      = "About %s %s %s %s ago";
			$month_label = $res['month'] > 1 ? 'months' : 'month';
			$day_label   = $res['day'] > 1 ? 'days' : 'day';
			return sprintf($format, $res['month'], $month_label, $res['day'], $day_label);
		} else {
			$format      = "About %s %s ago";
			$month_label = $res['month'] > 1 ? 'months' : 'month';
			return sprintf($format, $res['month'], $month_label);
		}
	}

	if (isset($res['day']) && $res['day'] > 0) {
		if ($res['day'] == 1) {
			return sprintf("Yesterday at %s", date('h:i a', $argtime));
		}
		if ($res['day'] <= 7) {
			return date("\L\a\s\\t l \a\\t h:i a", $argtime);
		}
		if ($res['day'] <= 31) {
			return date("l \a\\t h:i a", $argtime);
		}
	}

	if (isset($res['hour']) && $res['hour'] > 0) {
		if ($res['hour'] > 1) {
			return sprintf("About %s hours ago", $res['hour']);
		} else {
			return "About an hour ago";
		}
	}

	if (isset($res['min']) && $res['min']) {
		if ($res['min'] == 1) {
			return "About one minute ago";
		} else {
			return sprintf("About %s minutes ago", $res['min']);
		}
	}

	if (isset ($res['sec']) && $res['sec'] > 0) {
		if ($res['sec'] == 1) {
			return "One second ago";
		} else {
			return sprintf("%s seconds ago", $res['sec']);
		}
	}
}

function format_tweet($txt){
	$t = htmlentities($txt, ENT_QUOTES, 'utf-8');
	$t = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a href="$1" class="hash">$1</a>', $t);
	$t = preg_replace('/@(\w+)/', '<a href="http://twitter.com/$1" target="_blank" class="mention">@$1</a>', $t);
	$t = preg_replace('/\s#(\w+)/', ' <a href="http://twitter.com/search?q=%23$1" target="_blank" class="hash" rel="nofollow">#$1</a>', $t);
	$t = preg_replace('/^#(\w+)/', '<a href="http://twitter.com/search?q=%23$1" target="_blank" class="hash" rel="nofollow">#$1</a>', $t);
	return $t;
}
