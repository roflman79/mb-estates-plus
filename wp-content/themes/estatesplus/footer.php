  <footer id="main-footer">
    <p><?php the_field('address', 'option'); ?><br>
    Tel <?php the_field('telephone', 'option'); ?>  Email <?php echo make_clickable( get_field('email_address', 'option') ); ?></p>
    <?php

    $args = array(
      'menu' => 'Footer Menu',
      'container' => '',
      'depth' => 0
    );
    wp_nav_menu($args);

    ?>
  </footer>
</div>

<?php wp_footer(); ?>
<script type="text/javascript">
if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-44335506-1', 'mbestatesplus.com');
  ga('send', 'pageview');
}
</script>
</body>
</html>