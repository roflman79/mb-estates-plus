<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>

    <section class="content">

      <section id="splash" class="cycle-slideshow" 
      data-cycle-fx="scrollHorz" 
      data-cycle-pause-on-hover="true"
      data-cycle-slides="> .slide">
        <div class="cycle-pager"></div>
        <?php while( has_sub_field('carousel') ): ?>
        <div class="slide">
          <?php
          $posts = get_sub_field('link');
          if( $posts):
            foreach( $posts as $post):
              setup_postdata($post);
              $url = get_permalink();
            endforeach; wp_reset_postdata();
          endif;
          $img = get_sub_field('photo');
          ?>
          <img src="<?php echo $img['sizes']['square'] ?>" alt="" />
          <article class="<?php the_sub_field('colour_scheme'); ?>">
            <h1><?php the_sub_field('title'); ?></h1>
            <?php if( get_sub_field('caption') ): ?><h2><?php the_sub_field('caption'); ?></h2><?php endif; ?>
            <?php the_sub_field('content'); ?>
            <a href="<?php echo $url; ?>" class="btn">Learn More</a>
          </article>
        </div>
        <?php endwhile; ?>
      </section>
      <section class="widget grey" id="newsletter">
        <div>
          <header>Our newsletter</header>
          <p class="lrg">The latest news and views, straight to your inbox</p>
          <a href="#" class="btn">Register Now</a>
        </div>
        <img src="<?php bloginfo('template_directory'); ?>/img/newsletter.jpg" alt="" />
        <span></span>
      </section>
      <section class="widget black" id="contact">
        <div>
          <header>Get in touch</header>
          <ul>
            <li><span>Call us</span> <?php the_field('telephone', 'option'); ?></li>
            <li><span>Email</span> <a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a></li>
            <li><span>Visit us</span> <a href="<?php echo get_permalink(19); ?>" class="grey">view Google map</a></li> 
          </ul>
          <a href="<?php echo get_permalink(19); ?>" class="btn">Contact us</a>
        </div>
      </section>
      <section class="widget red" id="twitter">
        <div>
          <header>Twitter</header>
          <div id="tweets">
            <?php get_tweets( 'MBEstatesPlus',1 ); ?>
          </div>
          <a href="http://twitter.com/MBEstatesPlus" target="_blank" class="btn">Follow us</a>
        </div>
      </section>
      
    </section>
    
    <?php endwhile; endif; ?>

<?php get_footer(); ?>