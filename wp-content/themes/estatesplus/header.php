<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php
  global $page, $paged;
  wp_title( '|', true, 'right' );
  bloginfo( 'name' );
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";
  ?></title>
  <?php wp_head(); ?>
</head>
<body <?php body_class(get_field('colour_scheme')); ?>>
  
  <div class="container">
    <header id="main-header">
      <nav class="global">
        <?php

        $args = array(
          'menu' => 'Main Menu',
          'container' => '',
          'depth' => 0
        );
        wp_nav_menu($args);

        ?>
      </nav>
      <a href="<?php bloginfo('url'); ?>" id="logo"><?php bloginfo('name'); ?></a>
    </header>